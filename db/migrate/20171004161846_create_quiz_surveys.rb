class CreateQuizSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :quiz_surveys do |t|
      t.string :name
      t.integer :correct_options

      t.timestamps
    end
  end
end
