question1 = Quiz::Question.create(description: 'Inkitt is the World’s First Reader-Powered Book Publisher?', order: 1)
question1.options.create(description: 'Yes', correct: true)
question1.options.create(description: 'No', correct: false)

question2 = Quiz::Question.create(description: 'What link is not default in the Inkitt home page?', order: 2)
question2.save
question2.options.create(description: 'theguardian', correct: false)
question2.options.create(description: 'Financial Times', correct: false)
question2.options.create(description: 'Ford', correct: true)
question2.options.create(description: 'the bookseller', correct: false)
question2.options.create(description: 'The wall street journal', correct: false)

question3 = Quiz::Question.create(description: 'Inkitt has no mobile application?', order: 3)
question3.save
question3.options.create(description: 'Yes', correct: false)
question3.options.create(description: 'No', correct: true)

question4 = Quiz::Question.create(description: 'What was the Novel Content July?', order: 4)
question4.save
question4.options.create(description: 'Sexy Greek God Kidnappers', correct: true)
question4.options.create(description: 'The inherited', correct: false)
question4.options.create(description: 'I Dare to Love', correct: false)
question4.options.create(description: 'Kissing Cousins', correct: false)
question4.options.create(description: 'The Enemy', correct: false)

question5 = Quiz::Question.create(description: 'The Inkitt’s page in facebook has more than 15.000 followers?', order: 5)
question5.save
question5.options.create(description: 'Yes', correct: true)
question5.options.create(description: 'No', correct: false)