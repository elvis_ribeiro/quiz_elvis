# README

I decide to use sqlite3

The initial data with questions and options is in the seed.

To store the survey information in the browser and avoid to user execute the quiz twice, I decide to use cookies.
Then you need to clear the browser cookies to redo the test or do from another browser to get information from more the one user.

I used byebug to execute the unit tests

# Suggested commands to create database
rails db:migrate db:seed