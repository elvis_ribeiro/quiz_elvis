class QuizController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @question_id = cookies[:question_id]

    if @question_id.to_f > 5 then
      #cookies.delete :survey_id
      #cookies.delete :question_id
      render "survey_complete"
    else
      @survey = get_survey
      @question = get_question
    end
  end

  def next
    @survey = get_survey
    @option_id = params[:quiz_option_id]
    @option = Quiz::Option.find(@option_id)

    @question_id = params[:quiz_question][:id].to_f + 1

    if @option_id then
      Quiz::AnsweredQuestion.create(option: @option, survey: @survey)

      @correct_options = @survey.correct_options
      if @option.correct then
        @correct_options += 1
      end
      @survey.correct_options = @correct_options
      @survey.save
    end

    if @question_id > 5 then
      @surveyResult = ActiveRecord::Base.connection.exec_query('select 100*sum(correct_options)/sum(5) as percent from quiz_surveys')
      @questionResult = ActiveRecord::Base.connection.exec_query("select 100*sum(case when quiz_options.correct = 't' then 1 else 0 end)/count(*) as percent from quiz_answered_questions inner join quiz_options on quiz_options.id = quiz_answered_questions.option_id group by quiz_options.question_id")

      cookies[:question_id] = {:value => @question_id, :expires => 2.years.from_now}
      render "survey_success"
    else
      @question = Quiz::Question.find(@question_id)
      cookies[:question_id] = {:value => @question.id, :expires => 2.years.from_now}
      render "index"
    end
  end

  def get_survey
    # Validate survey by cookie
    @survey_id = cookies[:survey_id]
    if @survey_id then
      @survey = Quiz::Survey.find(@survey_id)
    else
      @user = User.create(name: request.user_agent)
      @survey = Quiz::Survey.create(name: "Inkitt Quiz done by Elvis", correct_options: 0)
      cookies[:survey_id] = {:value => @survey.id, :expires => 2.years.from_now}
    end
    return @survey
  end

  def get_question
    # Validate survey by cookie
    @question_id = cookies[:question_id]
    puts @question_id
    if @question_id then
      @question = Quiz::Question.find(@question_id)
    else
      @question = Quiz::Question.find(1)
      cookies[:question_id] = {:value => @question.id, :expires => 2.years.from_now}
    end
    return @question
  end
end
