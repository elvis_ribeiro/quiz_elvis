class Quiz::AnsweredQuestion < ApplicationRecord
  belongs_to :survey, class_name: Quiz::Survey
  belongs_to :option, class_name: Quiz::Option
  has_one :question, through: :option
end
