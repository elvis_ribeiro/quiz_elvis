class Quiz::Survey < ApplicationRecord
  has_many :questions, class_name: Quiz::Question
  has_many :answered_questions, class_name: Quiz::AnsweredQuestion
end
