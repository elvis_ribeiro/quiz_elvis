Rails.application.routes.draw do
  get 'quiz/index'
  get "quiz/next" => "quiz#index"
  patch "quiz/next" => "quiz#next"

  root 'quiz#index'
end
